function table.copy2(t)
    local u = { }
    for k, v in pairs(t) do u[k] = v end
    return setmetatable(u, getmetatable(t))
end

function get_pos_and_rotate(pos, direction, plusorminus, startvalue, endvalue, itemstack, user, axis)

    positions = {}
    posabove = table.copy(pos)
    posabove["y"] = posabove["y"] + 1
    pointed_thing = {["type"] = "node", ["under"] = pos, ["above"] = posabove}
    screwdriver.handler(itemstack, user, pointed_thing, axis, 200)
    for i = startvalue, endvalue, 1
    do  
        temppos = table.copy(pos)
        if plusorminus == "+" then
            
            temppos[direction] = temppos[direction] + i
        else
            temppos[direction] = temppos[direction] - i
        end
        tempposabove = table.copy(temppos)
        tempposabove["y"] = tempposabove["y"] + 1
        pointed_thing = {["type"] = "node", ["under"] = temppos, ["above"] = tempposabove}
        screwdriver.handler(itemstack, user, pointed_thing, axis, 200)
        table.insert(positions, temppos)
    end
    
end

function make_pos(pos, look, itemstack, user, axis, range)

    positions = {}
    if look <= 45 or look >= 315 then
        get_pos_and_rotate(pos, "z", "+", 1, range, itemstack, user, axis)
    elseif look <= 135 then
        get_pos_and_rotate(pos, "x", "-", 1, range, itemstack, user, axis)
    elseif look <= 225 then
        get_pos_and_rotate(pos, "z", "-", 1, range, itemstack, user, axis)
    elseif look <= 315 then
        get_pos_and_rotate(pos, "x", "+", 1, range, itemstack, user, axis)

    end
end

function settings(user)
    minetest.show_formspec(user:get_player_name(), "batch_screwdriver:settings",
    "size[2.3,2]" ..
    "field[0.5,0.5;2,1;range;Range;]"..
    "button_exit[0.2,1;2,1;exit;Save]")
end

minetest.register_tool("batch_screwdriver:batch_screwdriver", {
    description = "Batch Screwdriver",
    inventory_image = "screwdriver.png^[colorize:#F200FF:90",
    on_use = function(itemstack, user, pointed_thing)
        local tool = user:get_wielded_item()
        local meta = tool:get_meta()

        if pointed_thing.type == "nothing" then
            settings(user)
        
        elseif pointed_thing.type == "node" then
            local range = meta:get_int("range")
            make_pos(pointed_thing.under, math.deg(user:get_look_horizontal()), itemstack, user, screwdriver.ROTATE_FACE, range-1)
        end
		return itemstack
	end,
	on_place = function(itemstack, user, pointed_thing)
        local tool = user:get_wielded_item()
        local meta = tool:get_meta()

        if pointed_thing.type == "nothing" then
            settings(user)
        elseif type == "node" then
            local range = meta:get_int("range")
            make_pos(pointed_thing.under, math.deg(user:get_look_horizontal()), itemstack, user, screwdriver.ROTATE_AXIS, range-1)
        end
		return itemstack
	end,
})

minetest.register_on_player_receive_fields(function(player, formname, fields)
    if formname ~= "batch_screwdriver:settings" then
        return false
    end

    local tool = player:get_wielded_item()
    local meta = tool:get_meta()
    if tonumber(fields.range) ~= nil then
        meta:set_int("range", fields.range)
    end
    player:set_wielded_item(tool)

    return true

end)